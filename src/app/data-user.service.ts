import { Injectable } from '@angular/core';
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { getDatabase, set, ref, get, child, remove } from "firebase/database";
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class DataUserService {

  appFirebase: any;
  userAuth: any;
  dataTipoDocs: any = [
    {
      COD_ITEM: "1-Tarjeta de identidad",
      NOM_ITEM: "Tarjeta de identidad"
    },
    {
      COD_ITEM: "2-Cédula de ciudadanía",
      NOM_ITEM: "Cédula de ciudadanía"
    }
  ];
  dataStatesVaccine: any = [
    {
      idStateVaccine: "1",
      nameStateVaccine: "VACUNA RESERVADA"
    },
    {
      idStateVaccine: "2",
      nameStateVaccine: "VACUNADO"
    }
  ];
  objTypeUserVaccine: any = [
    {
      idTypeUser: "1",
      nameTypeUser: "Administrador"
    },
    {
      idTypeUser: "2",
      nameTypeUser: "Usuario común"
    }
  ];
  placeVaccine: string = "PLAZA IMPERIAL";

  constructor
  (
    private alertCtrl: AlertController
  ) 
  {
    // Your web app's Firebase configuration
    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    const firebaseConfig = {
      apiKey: "AIzaSyDiEXCt5hFvLpKNZxAz0HyF7Qw7EwPue5Y",
      authDomain: "registercovid-a7996.firebaseapp.com",
      databaseURL: "https://registercovid-a7996-default-rtdb.firebaseio.com",
      projectId: "registercovid-a7996",
      storageBucket: "registercovid-a7996.appspot.com",
      messagingSenderId: "772834861650",
      appId: "1:772834861650:web:569aa982bfc962e7c6758b",
      measurementId: "G-CW7EWYEJVS"
    };  
    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    this.appFirebase = app;
    const analytics = getAnalytics(app);
    const auth = getAuth();
    signInWithEmailAndPassword(auth, "antonio_12065@hotmail.com", "Horseman65")
    .then((userCredential) => {
      this.userAuth = userCredential;
    })
    .catch((error) => {
      this.userAuth = null;
    });
  }

  async msjErrorAlert () {
    const alertErrorMsj = await this.alertCtrl.create({
      header: "Error al guardar",
      backdropDismiss: false,
      message: "Por favor comuniquese con el administrador de esta página y notifique este error.",
      buttons: [{
        text: "Aceptar",
        handler: () => {

        }
      }]
    });
    await alertErrorMsj.present();
  }

  async getDataUser (codDoc) {
    let objUser = {};
    const dbRef = ref(getDatabase());
    await get(child(dbRef, `usersVaccine/${codDoc}`)).then((snapshot) => {
      if (snapshot.exists()) {
        objUser = snapshot.val();
      } else {
        objUser = null;
      }
    }).catch((error) => {
      objUser = null;
    });
    return objUser;
  }

  async writeUserData(userId, objUser) {
    var logResponse = null;
    if (this.userAuth) {
      try {
        await set(ref(getDatabase(), 'usersVaccine/' + userId), objUser);
        logResponse = "OK";
      } catch (error) {
        logResponse = null;
      }
    }
    return logResponse;
  }

  async validateIdVaccine (idVaccine) {
    let flagExist = true;
    const dbRef = ref(getDatabase());
    await get(child(dbRef, `vaccineLog/${idVaccine}`)).then((snapshot) => {
      if (snapshot.exists()) {
        flagExist = true;
      } else {
        flagExist = false;
      }
    }).catch((error) => {
      flagExist = true;
    });
    return flagExist;
  }

  async writeVaccineLog (idVaccine, objLogvaccine, userId, objUser) {
    var logResponse = null;
    if (this.userAuth) {
      try {
        await set(ref(getDatabase(), 'vaccineLog/' + idVaccine), objLogvaccine);
        logResponse = await this.writeUserData(userId, objUser);
      } catch (error) {
        logResponse = null;
      }
    }
    return logResponse;
  }

  async getLogVaccine (idVaccineLog) {
    let objLogVaccine = {};
    const dbRef = ref(getDatabase());
    await get(child(dbRef, `vaccineLog/${idVaccineLog}`)).then((snapshot) => {
      if (snapshot.exists()) {
        objLogVaccine = snapshot.val();
      } else {
        objLogVaccine = null;
      }
    }).catch((error) => {
      objLogVaccine = null;
    });
    return objLogVaccine;
  }

  async getAllUsers () {
    var arrayUsers = {};
    const dbRef = ref(getDatabase());
    await get(child(dbRef, `usersVaccine`)).then((snapshot) => {
      if (snapshot.exists()) {
        arrayUsers = snapshot.val();
      } else {
        arrayUsers = null;
      }
    }).catch((error) => {
      arrayUsers = null;
    });
    return arrayUsers;
  }

  async removeData (idVaccine, codDoc) {
    const dbRef = ref(getDatabase());
    await remove(child(dbRef, `usersVaccine/` + codDoc));
    await remove(child(dbRef, `vaccineLog/` + idVaccine));
  }

}
