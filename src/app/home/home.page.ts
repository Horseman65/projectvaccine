import { Component } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AlertController, LoadingController } from '@ionic/angular';
import { DataUserService } from '../data-user.service';
import { NgbModal, NgbModalConfig, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  frmLogin = new FormGroup({});
  frmRegister = new FormGroup({});
  modal: NgbModalRef;
  contentModal: any;
  dataTipoDocs: any;
  flagEmailInvalid: boolean = false;

  constructor
  (
    private formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private dataUser: DataUserService,
    private config: NgbModalConfig,
    private modalService: NgbModal,
    private router: Router
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
    config.centered = true;
    this.frmLogin = this.formBuilder.group({
      nDoc : ['', Validators.required]
    });
    this.frmRegister = this.formBuilder.group({
      typeDoc: ['', Validators.required],
      codDoc: ['', Validators.required],
      nameUser: ['', Validators.required],
      surname_1: ['', Validators.required],
      surname_2: ['', null],
      address: ['', Validators.required],
      phone: ['', null],
      cellphone: ['', Validators.required],
      email: ['', Validators.required]
    });
  }

  ionViewWillEnter () {
    localStorage.removeItem('admin');
    this.frmLogin.reset();
    this.frmRegister.reset();
    this.contentModal = null;
    this.flagEmailInvalid = false;
    this.dataTipoDocs = this.dataUser.dataTipoDocs;
  }

  numberOnlyValidation (event: any) {
    const pattern = /[\(\d)]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  /* Método encargado de validar email */
  validateEmail () {
    var valueEmail = this.frmRegister.get('email').value;
    const pattern = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/;
    if (pattern.test(valueEmail)) {
      this.flagEmailInvalid = false;
    } else {
      this.flagEmailInvalid = true;
    }
  }

  async validateStateVaccine (contentModal) {
    this.contentModal = contentModal;
    var nDoc = this.frmLogin.get('nDoc').value;
    await this.loadingCtrl.create({
      message: "Validando número de documento..."
    }).then((res) => {
      res.present();
    });
    let objUser = await this.dataUser.getDataUser(nDoc);
    if (objUser) {
      if (objUser['typeUser']['idTypeUser'] === this.dataUser.objTypeUserVaccine[1]['idTypeUser']) {
        let objVaccineLog = await this.dataUser.getLogVaccine(objUser['idVaccineLog']);
        if (objVaccineLog) {
          if (objVaccineLog['stateVaccine']['idStateVaccine'] === this.dataUser.dataStatesVaccine[0]['idStateVaccine']) {
            let dateView = moment(objVaccineLog['dateRegister'], 'YYYY-MM-DDThh:mm:ss').format('DD/MM/YYYY');
            let hourDateView = moment(objVaccineLog['dateRegister'], 'YYYY-MM-DDThh:mm:ss').format('hh:mm A');
            const alertVacReg = await this.alertCtrl.create({
              header: "Vacuna reservada",
              backdropDismiss: false,
              message: "La fecha de su vacuna es para el día " + dateView + " en las " + hourDateView + " en Plaza Imperial.</br>No olvide llevar su documento de identidad.",
              buttons: [{
                text: "Aceptar",
                handler: () => {

                }
              }]
            });
            await alertVacReg.present();
          } else if (objVaccineLog['stateVaccine']['idStateVaccine'] === this.dataUser.dataStatesVaccine[1]['idStateVaccine']) {
            const alertUserVac = await this.alertCtrl.create({
              header: "Usuario ya vacunado",
              backdropDismiss: false,
              message: "Usted ya se encuentra vacunado.",
              buttons: [{
                text: "Aceptar",
                handler: () => {

                }
              }]
            });
            await alertUserVac.present();
          }
        }
      } else if (objUser['typeUser']['idTypeUser'] === this.dataUser.objTypeUserVaccine[0]['idTypeUser']) {
        localStorage.setItem('admin', 'true');
        this.router.navigate(['./page-admin']);
      }
    } else {
      const alertNoData = await this.alertCtrl.create({
        header: "Usuario no registrado",
        backdropDismiss: false,
        message: "Actualmente no estás registrado, desea registrarse?",
        buttons: [{
          text: "Aceptar",
          handler: () => {
            this.frmRegister.get('codDoc').patchValue(nDoc);
            this.modal = this.modalService.open(this.contentModal, { ariaLabelledBy: 'modal-basic-title' });
          }
        }, {
          text: "Cancelar",
          handler: () => {

          }
        }]
      });
      await alertNoData.present();
    }
    this.loadingCtrl.dismiss();
  }

  async registerUser () {
    await this.loadingCtrl.create({
      message: "Registrando usuario..."
    }).then((res) => {
      res.present();
    });
    let codDoc = this.frmRegister.get("codDoc").value;
    let objUser = await this.dataUser.getDataUser(codDoc);
    this.modal.dismiss();
    if (objUser) {
      this.config.backdrop = false;
      const alertUserExist = await this.alertCtrl.create({
        header: "Usuario ya registrado",
        backdropDismiss: false,
        message: "Actualmente ya se encuentra un usuario registrado con ese número de documento.</br>Por favor intente con otro número de documento.",
        buttons: [{
          text: "Aceptar",
          handler: () => {
            this.modal = this.modalService.open(this.contentModal, { ariaLabelledBy: 'modal-basic-title' });
          }
        }]
      });
      await alertUserExist.present();
    } else {
      var objDataUser = {};
      objDataUser["typeUser"] = this.dataUser.objTypeUserVaccine[1];
      for (let field of Object.keys(this.frmRegister.controls)) {
        if (field !== "codDoc" && field !== "typeDoc") {
          objDataUser[field] = this.frmRegister.get(field).value;
        } else if (field === "typeDoc") {
          var dataTypeDoc = this.frmRegister.get(field).value;
          var arrayDataTypeDoc = dataTypeDoc.split("-");
          objDataUser[field] = {
            idTypeDoc: arrayDataTypeDoc[0],
            nameTypeDoc: arrayDataTypeDoc[1]
          };
        }
      }
      let flagExist = true;
      let idVaccine;
      while (flagExist) {
        idVaccine = btoa(Math.random().toString());
        flagExist = await this.dataUser.validateIdVaccine(idVaccine);
      }
      objDataUser['idVaccineLog'] = idVaccine;
      let objVaccineLog = {};
      let weekFuture = moment().add(7, 'day');
      weekFuture.format('DD MM YYYY, h:mm:ss a');
      let dateView = moment(weekFuture, 'DD MM YYYY, h:mm:ss a').format('DD/MM/YYYY');
      let hourDateView = moment(weekFuture, 'DD MM YYYY, h:mm:ss a').format('hh:mm A');
      objVaccineLog = {
        idCodDocUser: codDoc,
        stateVaccine: this.dataUser.dataStatesVaccine[0],
        dateRegister: weekFuture.format('YYYY-MM-DDThh:mm:ss'),
        placeVaccine: this.dataUser.placeVaccine
      };
      let logResponse = await this.dataUser.writeVaccineLog(idVaccine, objVaccineLog, codDoc, objDataUser);      
      if (logResponse) {
        const alertRegExit = await this.alertCtrl.create({
          header: "Vacuna reservada",
          backdropDismiss: false,
          message: "Su vacuna fue reservada para el día " + dateView + " a las " + hourDateView + " en Plaza Imperial.</br>No olvide llevar su documento de identidad.",
          buttons: [{
            text: "Aceptar",
            handler: () => {
              
            }
          }]
        });
        await alertRegExit.present();
      } else {
        await this.dataUser.msjErrorAlert();
      }
      this.frmRegister.reset();
    }
    this.loadingCtrl.dismiss();
  }

}
