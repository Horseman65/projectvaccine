import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageAdminPage } from './page-admin.page';

const routes: Routes = [
  {
    path: '',
    component: PageAdminPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PageAdminPageRoutingModule {}
