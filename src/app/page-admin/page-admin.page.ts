import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AlertController, LoadingController } from '@ionic/angular';
import { DataUserService } from '../data-user.service';
import { NgbModal, NgbModalConfig, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-page-admin',
  templateUrl: './page-admin.page.html',
  styleUrls: ['./page-admin.page.scss'],
})
export class PageAdminPage implements OnInit {

  arrayUsersWithLog: any = [];
  frmEditUser = new FormGroup({});
  modal: NgbModalRef;
  contentModal: any;
  dataTipoDocs: any = [];
  dataStatesVaccine: any = [];
  userEdit: any = {};

  constructor
  (
    private formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private dataUser: DataUserService,
    private config: NgbModalConfig,
    private modalService: NgbModal,
    private router: Router
  ) { 
    config.backdrop = 'static';
    config.keyboard = false;
    config.centered = true;
    this.frmEditUser = this.formBuilder.group({
      typeDoc: ['', Validators.required],
      codDoc: ['', Validators.required],
      nameUser: ['', Validators.required],
      surname_1: ['', Validators.required],
      surname_2: ['', null],
      address: ['', Validators.required],
      cellphone: ['', Validators.required],
      stateVaccine: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  async ionViewWillEnter () {
    if (localStorage.getItem('admin') === 'true') {
      this.userEdit = {};
      this.frmEditUser.reset();
      this.arrayUsersWithLog = [];
      await this.getUsersVaccine();
      this.contentModal = null;
      this.dataTipoDocs = this.dataUser.dataTipoDocs;
      this.dataStatesVaccine = this.dataUser.dataStatesVaccine;
    } else {
      this.router.navigate(['./home']);
    }
  }

  numberOnlyValidation (event: any) {
    const pattern = /[\(\d)]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  async getUsersVaccine () {
    this.arrayUsersWithLog = [];
    let arrayUsers = await this.dataUser.getAllUsers();
    if (arrayUsers) {
      for (let user of Object.keys(arrayUsers)) {
        if (arrayUsers[user]['idVaccineLog']) {
          var objUser = {
            codDocUser: user
          };
          objUser = Object.assign(objUser, arrayUsers[user]);
          let objLogVaccine = await this.dataUser.getLogVaccine(arrayUsers[user]['idVaccineLog']);
          objUser['objVaccineLog'] = objLogVaccine;
          this.arrayUsersWithLog.push(objUser);
        }
      }
      console.log(this.arrayUsersWithLog);
    }
  }

  openModalEditUser (contentModal, user) {
    this.userEdit = user;
    this.contentModal = contentModal;
    this.modal = this.modalService.open(this.contentModal, { ariaLabelledBy: 'modal-basic-title' });
    for (let infoUser of Object.keys(user)) {
      if (infoUser === 'codDocUser') {
        this.frmEditUser.get('codDoc').patchValue(user[infoUser]);
      } else if (infoUser === 'typeDoc') {
        this.frmEditUser.get(infoUser).patchValue(user[infoUser]["idTypeDoc"] + "-" + user[infoUser]["nameTypeDoc"]);
      } else if (infoUser === 'objVaccineLog') {
        this.frmEditUser.get("stateVaccine").patchValue(user[infoUser]["stateVaccine"]["idStateVaccine"]);
      } else {
        if (this.frmEditUser.get(infoUser)) {
          this.frmEditUser.get(infoUser).patchValue(user[infoUser]);
        }
      }
    }
  }

  exitPage () {
    localStorage.removeItem('admin');
    this.router.navigate(['./home']);
  }

  async removeData (user) {
    await this.loadingCtrl.create({
      message: "Editando usuario..."
    }).then((res) => {
      res.present();
    });
    this.dataUser.removeData(user.idVaccineLog, user.codDocUser);
    const alerEditExt = await this.alertCtrl.create({
      header: "Usuario eliminado",
      backdropDismiss: false,
      message: "Usuario eliminado correctamente.",
      buttons: [{
        text: "Aceptar",
        handler: () => {
          
        }
      }]
    });
    await alerEditExt.present();
    this.loadingCtrl.dismiss();
    await this.getUsersVaccine();
  }

  async editUser () {
    this.modal.dismiss();
    await this.loadingCtrl.create({
      message: "Editando usuario..."
    }).then((res) => {
      res.present();
    });
    for (let field of Object.keys(this.frmEditUser.controls)) {
      if (this.userEdit[field]) {
        if (field !== 'typeDoc' && field !== 'codDoc') {
          this.userEdit[field] = this.frmEditUser.get(field).value;
        }
      } else if (field === 'stateVaccine') {
        this.userEdit.objVaccineLog.idStateVaccine = this.frmEditUser.get(field).value;
      }
    }
    let idVaccine = this.userEdit.idVaccineLog;
    let codDoc = this.userEdit.codDocUser;
    var objVaccineLog = {
      idCodDocUser: this.userEdit.codDocUser,
      dateRegister: this.userEdit.objVaccineLog.dateRegister,
      placeVaccine: this.dataUser.placeVaccine
    };
    var objDataUser = {
      address: this.userEdit.address,
      cellphone: this.userEdit.cellphone,
      email: this.userEdit.email,
      idVaccineLog: this.userEdit.idVaccineLog,
      nameUser: this.userEdit.nameUser,
      phone: this.userEdit.phone,
      surname_1: this.userEdit.surname_1,
      surname_2: this.userEdit.surname_2,
      typeDoc: this.userEdit.typeDoc,
      typeUser: this.userEdit.typeUser
    };
    if (this.userEdit.objVaccineLog.idStateVaccine === this.dataUser.dataStatesVaccine[1].idStateVaccine) {
      objVaccineLog['dateVaccine'] = moment().format('YYYY-MM-DDThh:mm:ss');
      objVaccineLog['stateVaccine'] = this.dataUser.dataStatesVaccine[1];
    } else if (this.userEdit.objVaccineLog.idStateVaccine === this.dataUser.dataStatesVaccine[0].idStateVaccine) {
      objVaccineLog['stateVaccine'] = this.dataUser.dataStatesVaccine[0];
    }
    let logResponse = await this.dataUser.writeVaccineLog(idVaccine, objVaccineLog, codDoc, objDataUser);      
    if (logResponse) {
      const alerEditExt = await this.alertCtrl.create({
        header: "Usuario editado",
        backdropDismiss: false,
        message: "Usuario editado correctamente.",
        buttons: [{
          text: "Aceptar",
          handler: () => {
            
          }
        }]
      });
      await alerEditExt.present();
    } else {
      await this.dataUser.msjErrorAlert();
    }
    this.userEdit = {};
    this.frmEditUser.reset();
    this.loadingCtrl.dismiss();
    await this.getUsersVaccine();
  }

}
