import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PageAdminPageRoutingModule } from './page-admin-routing.module';
import { PageAdminPage } from './page-admin.page';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PageAdminPageRoutingModule,
    ReactiveFormsModule,
    NgbModule
  ],
  declarations: [PageAdminPage]
})
export class PageAdminPageModule {}
